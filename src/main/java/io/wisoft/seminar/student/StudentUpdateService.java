package io.wisoft.seminar.student;

import io.wisoft.seminar.util.PgSqlAccess;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentUpdateService {



  public void updateStudentBirthday(final Student student) {
    final String query = "update student set birthday = ? where no = ?";

    try (Connection conn = PgSqlAccess.setConnection();
         PreparedStatement ps = conn.prepareStatement(query)) {
      conn.setAutoCommit(false);

      ps.setDate(1, Date.valueOf(student.getBirthday()));
      ps.setString(2, student.getNo());

      final int result = ps.executeUpdate();

      conn.commit();

      System.out.println(result + "건이 처리되었습니다.");
    } catch (SQLException e) {
      System.out.format("SQLException: %s, SQLState: %s", e.getMessage(), e.getSQLState());
    }
  }

  public void updateStudentBirthdayMultiBatch(final Student[] students) {
    final String query = "update student set birthday = ? where no = ?";

    try (Connection conn = PgSqlAccess.setConnection();
         PreparedStatement ps = conn.prepareStatement(query)) {
      conn.setAutoCommit(false);

      for (Student student: students) {

        ps.setDate(1, Date.valueOf(student.getBirthday()));
        ps.setString(2, student.getNo());

        ps.addBatch();
        ps.clearParameters();
      }

      final int[] result = ps.executeBatch();
      conn.commit();

      System.out.println(result.length + "건이 처리되었습니다.");
    } catch (SQLException e) {
      System.out.format("SQLException: %s, SQLState: %s", e.getMessage(), e.getSQLState());
    }
  }

}
