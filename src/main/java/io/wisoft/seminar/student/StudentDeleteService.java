package io.wisoft.seminar.student;

import io.wisoft.seminar.util.PgSqlAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentDeleteService {

  public void deleteStudentNo(final String no) {
    final String query = "delete from student where no = ?";

    try (Connection conn = PgSqlAccess.setConnection();
         PreparedStatement ps = conn.prepareStatement(query)) {
      conn.setAutoCommit(false);

      ps.setString(1, no);

      final int result = ps.executeUpdate();

      conn.commit();

      System.out.println(result + "건이 처리되었습니다.");

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void deleteStudentMultiBatch(final String[] numbers) {
    final String query = "delete from student where no = ?";

    try (Connection conn = PgSqlAccess.setConnection();
         PreparedStatement ps = conn.prepareStatement(query)) {
      conn.setAutoCommit(false);

      for (String number : numbers) {
        ps.setString(1, number);

        ps.addBatch();
        ps.clearParameters();
      }

      final int[] result = ps.executeBatch();

      conn.commit();

      System.out.println(result.length + "건이 처리되었습니다.");

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
